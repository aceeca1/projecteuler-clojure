#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(time (println (apply +
    (for [i (str (apply * (range 1N 101N)))] (- (int i) (int \0))))))

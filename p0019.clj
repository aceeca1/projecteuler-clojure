#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(time (println (count (for [year (range 1901 2001) month (range 0 12)
    :let [a (java.util.GregorianCalendar. year month 1)]
    :let [a (.get a java.util.Calendar/DAY_OF_WEEK)]
    :when (== java.util.Calendar/SUNDAY a)] nil))))

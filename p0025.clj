#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(time (println (int (Math/ceil (/
    (+ 999.0 (Math/log10 (Math/sqrt 5)))
    (Math/log10 (/ (+ 1.0 (Math/sqrt 5)) 2)))))))

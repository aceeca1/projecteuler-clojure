#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(time (println (apply + (map-indexed
    (fn [^long k v] (* (inc k)
        ^long (apply + (for [i v] (- (int i) (int \A) -1)))))
    (sort (vec (for [^String i
        (clojure.string/split-lines (slurp "data/p0022"))]
        (subs i 1 (- (.length i) 2)))))))))

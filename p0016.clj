#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(time (println (apply + (for [i (str (loop [n 1000 ans 1N]
    (if (zero? n) ans (recur (dec n) (.add ans ans)))))] (- (int i) (int \0))))))

#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(comment Snippet factorize)
(defn factorize [^long n] (loop [i 2 n n ans []] (cond
    (> (* i i) n) (if (> n 1) (conj ans n) ans)
    (zero? (rem n i)) (recur i (quot n i) (conj ans i))
    :else (recur (case i 2 3 (inc i)) n ans))))

(time (println (last (factorize 600851475143))))

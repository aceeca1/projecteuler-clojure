#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn at [^String s i] (let [ans (- (int (.charAt s i)) (int \0))]
    (if (zero? ans) [1 1] [ans 0])))

(time (println (loop [k 13 i 0 prod 1 zeros 0 ans 0
    s (clojure.string/replace (slurp "data/p0008") #"[^\d]" "")] (if
        (>= i (.length s)) ans
        (let [[^long si ^long zi] (at s i)
            [^long sk ^long zk] (if (< i k) [1 0] (at s (- i k)))]
        (recur k (inc i)
            (* si (quot prod sk)) (+ zi (- zeros zk))
            (if (zero? zeros) (max ans prod) ans) s))))))

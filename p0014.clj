#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn f ^long [^long n] (if (even? n) (quot n 2) (inc (* n 3))))

(time (println (loop [n 1000000 a (doto (int-array n) (aset 1 1)) ans 0 i 1]
    (if (>= i n) ans (let [^long ai
        (loop [j i ans 0] (if
            (and (< j n) (not (zero? (aget a j)))) (+ ans (aget a j))
            (recur (f j) (inc ans))))]
        (loop [j i ans ai] (cond
            (>= j n) (recur (f j) (dec ans))
            (zero? (aget a j))
                (do (aset a j ans) (recur (f j) (dec ans)))))
        (recur n a (if (> ai (aget a ans)) i ans) (inc i)))))))

#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(comment Snippet get-prime)
(defn get-prime [^long n] (loop [p (int-array (inc n)) u 0 i 2] (if (> i n)
    (do (aset p u (inc n)) p)
    (let [pi (aget p i) ^long v (if (zero? pi) (do (aset p u i) i) pi)]
        (loop [w 2] (and (<= (* i w) n) (aset p (* i w) w) (< w v)
            (recur (aget p w))))
        (recur p (if (zero? pi) i u) (inc i))))))

(time (println (let [n 10000
    m (int (* n (Math/log n) 1.5)) ^ints p (get-prime m)]
    (loop [n n i 2] (if (zero? n) i (recur (dec n) (aget p i)))))))

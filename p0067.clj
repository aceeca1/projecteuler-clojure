#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(time (println (first (reduce
    (fn [a1 a0] (vec (map-indexed
        #(+ ^long %2 (max ^long (a1 %) ^long (a1 (inc ^long %)))) a0)))
    (rseq (vec (for [i (clojure.string/split-lines (slurp "data/p0067"))]
        (vec (for [j (clojure.string/split i #" ")]
            (Integer/parseInt j))))))))))

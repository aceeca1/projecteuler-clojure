#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(time (println (loop [a0 1 a1 1 ans 0] (if (>= a0 4000000) ans
    (recur (+ a0 a1) a0 (if (even? a0) (+ ans a0) ans))))))

#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(comment Snippet gcd)
(defn gcd ^long [^long n1 ^long n2] (if (zero? n2) n1 (recur n2 (rem n1 n2))))

(defn lcm ^long [^long n1 ^long n2] (* (quot n1 (gcd n1 n2)) n2))

(time (println (reduce lcm (range 2 21))))

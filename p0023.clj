#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(comment Snippet sum-of-divisors)
(defn sum-of-divisors ^long [^long n] (inc (long (loop [i 2 ans 0] (cond
    (< (* i i) n) (recur (inc i)
        (if (zero? (unchecked-remainder-int n i)) (+ ans i (quot n i)) ans))
    (== (* i i) n) (+ ans i) :else ans)))))

(time (println (let [n 28124 b (java.util.BitSet. n)
    a (vec (for [^long i (range 1 n) :when (> (sum-of-divisors i) i)] i))
    two-sum? (fn [^long m] (reduce #(cond
        (> ^long %2 m) (reduced false)
        (.get b (- m ^long %2)) (reduced true)) nil a))]
    (doseq [i a] (.set b i))
    (apply + (for [i (range 1 n) :when (not (two-sum? i))] i)))))

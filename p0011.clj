#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(time (println (let [
    s (vec (for [i (clojure.string/split-lines (slurp "data/p0011"))]
        (vec (for [j (clojure.string/split i #" ")] (Integer/parseInt j)))))
    n (count s) m (count (first s))]
    (apply max (for [
        [^long dx ^long dy] [[0 1] [1 0] [1 1] [1 -1]]
        ^long x (range n) ^long y (range m)
        :when (and (< (+ x (* 3 dx)) n) (< -1 (+ y (* 3 dy)) m))]
        (apply * (for [^long i (range 4)]
            ((s (+ x (* i dx))) (+ y (* i dy))))))))))

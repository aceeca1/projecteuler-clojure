#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(comment Snippet num-of-divisors)
(defn num-of-divisors ^long [^long n] (loop [i 1 ans 0] (cond
    (< (* i i) n) (recur (inc i)
        (if (zero? (unchecked-remainder-int n i)) (+ ans 2) ans))
    (== (* i i) n) (inc ans) :else ans)))

(time (println (loop [i 1 np 0]
    (let [n (num-of-divisors (if (even? i) (quot i 2) i))]
        (if (> (* n np) 500) (quot (* i (dec i)) 2) (recur (inc i) n))))))

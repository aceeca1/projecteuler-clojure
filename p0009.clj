#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(time (println (let [u 500] (first
    (for [^long m (range 1 (inc u))
        :when (zero? (rem u m)) :let [kp (quot u m)]
        ^long p (range (inc m) (+ m m)) :when (zero? (rem kp p))
        :let [k (quot kp p) n (- p m) m2 (* m m) n2 (* n n)
            a (* k (- m2 n2)) b (* k m n 2) c (* k (+ m2 n2))]] (* a b c))))))

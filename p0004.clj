#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(time (println (apply max (for [
    ^long i (range 100 1000) ^long j (range 100 1000)
    :let [k (* i j) s (str k)] :when (= s (clojure.string/reverse s))] k))))


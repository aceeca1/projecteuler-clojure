#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn pron ^long [^long n] (case n
    1  (.length "one")       2  (.length "two")        3  (.length "three")
    4  (.length "four")      5  (.length "five")       6  (.length "six")
    7  (.length "seven")     8  (.length "eight")      9  (.length "nine")
    10 (.length "ten")       11 (.length "eleven")     12 (.length "twelve")
    13 (.length "thirteen")  14 (.length "fourteen")   15 (.length "fifteen")
    16 (.length "sixteen")   17 (.length "seventeen")  18 (.length "eighteen")
    19 (.length "nineteen")  20 (.length "twenty")     30 (.length "thirty")
    40 (.length "forty")     50 (.length "fifty")      60 (.length "sixty")
    70 (.length "seventy")   80 (.length "eighty")     90 (.length "ninety")
    1000 (.length "oneThousand") (cond
        (< n 100) (+ (pron (* (quot n 10) 10)) (pron (rem n 10)))
        (zero? (rem n 100)) (+ (pron (quot n 100)) (.length "hundred"))
        :else (+ (pron (* (quot n 100) 100))
            (.length "and") (pron (rem n 100))))))

(time (println (apply + (for [i (range 1 1001)] (pron i)))))

#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(defn square ^long [^long n] (* n n))

(time (println (let [n 101] (-
    ^long (square (apply + (range 1 n)))
    ^long (apply + (map square (range 1 n)))))))

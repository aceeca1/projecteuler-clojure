#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(comment Snippet find-cycle)
(defn find-cycle [f n] (loop [n1 0 a1 n n2 1 a2 (f n)] (cond
    (= a1 a2) (- n2 n1)
    (> n2 (+ n1 n1)) (recur n2 a2 (inc n2) (f a2))
    :else (recur n1 a1 (inc n2) (f a2)))))

(defn fraction-cycle [^long n] (find-cycle #(rem (* ^long % 10) n) 1))

(time (println (apply max-key fraction-cycle (range 1 1000))))

#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(time (println (let [
    s (clojure.string/split-lines (slurp "data/p0013"))
    s (str (apply + (for [i s] (Double/parseDouble i))))]
    (str (subs s 0 1) (subs s 2 11)))))

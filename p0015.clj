#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(comment Snippet choose)
(defn choose ^long [^long n ^long k] (loop [i1 n i2 1 ans 1] (if (> i2 k) ans
    (recur (dec i1) (inc i2) (quot (* ans i1) i2)))))

(time (println (choose 40 20)))

#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(time (do (doseq [i (sort (.list (clojure.java.io/file ".")
    (reify java.io.FilenameFilter (accept [this dir itsName] (and
        (.endsWith itsName ".clj")
        (not (.endsWith ^String *file* itsName)))))))]
    (println) (print i ": ") (load-file i)) (println "\nSummary:")))

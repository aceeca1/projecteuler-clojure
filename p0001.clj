#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(time (println (apply + (for [^long i (range 1000)
    :when (or (zero? (rem i 3)) (zero? (rem i 5)))] i))))

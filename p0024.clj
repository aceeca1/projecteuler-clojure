#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(comment Snippet but-k)
(defn but-k [s ^long k] (vec (concat (subvec s 0 k) (subvec s (inc k)))))

(time (println (apply str (first (reduce
    (fn [[s unused] i] [(conj s (unused i)) (but-k unused i)])
    [[] (vec (range 10))]
    (rseq (loop [n 999999 p [] i 1] (if (> i 10) p
        (recur (quot n i) (conj p (rem n i)) (inc i))))))))))

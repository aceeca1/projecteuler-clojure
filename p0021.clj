#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(comment Snippet sum-of-divisors)
(defn sum-of-divisors ^long [^long n] (inc (long (loop [i 2 ans 0] (cond
    (< (* i i) n) (recur (inc i)
        (if (zero? (unchecked-remainder-int n i)) (+ ans i (quot n i)) ans))
    (== (* i i) n) (+ ans i) :else ans)))))

(time (println (apply + (for [
    ^long i (range 10000) :let [k (sum-of-divisors i)]
    :when (and (not= k i) (== i (sum-of-divisors k)))] i))))

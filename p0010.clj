#!/usr/bin/clojure
(set! *warn-on-reflection* true)
(set! *unchecked-math* :warn-on-boxed)

(comment Snippet get-prime)
(defn get-prime [^long n] (loop [p (int-array (inc n)) u 0 i 2] (if (> i n)
    (do (aset p u (inc n)) p)
    (let [pi (aget p i) ^long v (if (zero? pi) (do (aset p u i) i) pi)]
        (loop [w 2] (and (<= (* i w) n) (aset p (* i w) w) (< w v)
            (recur (aget p w))))
        (recur p (if (zero? pi) i u) (inc i))))))

(time (println (let [n 2000000 p ^ints (get-prime n)]
    (loop [i 2 ans 0] (if (> i n) ans (recur (aget p i) (+ ans i)))))))
